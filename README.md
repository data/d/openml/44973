# OpenML dataset: grid_stability

https://www.openml.org/d/44973

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

The local stability analysis of the 4-node star system (electricity producer is in the center) implementing Decentral Smart Grid Control concept was performed.

This dataset contains simulations regarding electrical grid stability. The model is composed of a generator model and an economic model.

The analysis is performed for different sets of input values. Several input values are kept the same: averaging time - 2s, coupling strength - 8s^-2, damping - 0.1s^-1.

The goal is to estimate the stability of the system.

**Attribute Description**

14 features describing the system:

1. *tau[1-4]* - reaction time of participant (real from the range [0.5,10]s), tau1 - value for electricity producer
2. *p[1-4]* - nominal power consumed(negative) / produced(positive)(real). For consumers from the range [-0.5,-2]s^-2; p1 = abs(p2 + p3 + p4)
3. *g[1-4]* - coefficient (gamma) proportional to price elasticity (real from the range [0.05,1]s^-1), g1 - the value for electricity producer
4. *stab* - the maximal real part of the characteristic equation root (if positive - the system is linearly unstable), target feature
5. *stabf* - the stability label of the system (categorical: stable/unstable), alternate target feature for a classification task

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44973) of an [OpenML dataset](https://www.openml.org/d/44973). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44973/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44973/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44973/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

